from setuptools import setup

setup(
    name='Ryuuko',
    version='0.0.1',
    description='Image indexing server with JSON API',
    author='Matthew Summers',
    author_email='matt@wishf.co.uk',

    packages=['ryuuko'],

    install_requires=[
        'flask',
        'flask-login',
        'flask-wtf',
        'flask-sqlalchemy',
        'Flask-Bootstrap',
        'Flask-HTTPauth',
        'colorama',
        'passlib',
        'requests'
    ],

    entry_points={
        'console_scripts': [
            'ryuuko=ryuuko.tools.management:main',
            'ryuuko-pony=ryuuko.tools.client_test:main',
            'ryuuko-import=ryuuko.tools.import:main'
        ],
    }
)