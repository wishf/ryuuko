from flask.views import MethodView
from flask import Blueprint, jsonify, abort, g, request, current_app, send_file
import ryuuko.models as models
from ryuuko import db
from hashlib import sha512
import json
from ryuuko.authentication import authed
from binascii import hexlify
import os


class Category(MethodView):

    # Retrieve
    # All if no ID; else one
    def get(self, id):
        if id is None:
            dic = {str(v.id): v.name for k, v in enumerate(models.Category.query.all())}
            return jsonify(**dic)

        cat = models.Category.query.filter_by(id=id).first()

        if not cat:
            return abort(404)

        return jsonify(name=cat.name)

    # Create
    def post(self):
        js = request.get_json()

        if 'name' not in js:
            return abort(400)

        cat = models.Category(js['name'])

        g.user.categories.append(cat)

        db.session.add(cat)
        db.session.commit()

        return jsonify(id=cat.id, name=cat.name)

    # Update
    def put(self, id):
        js = request.get_json()

        if 'name' not in js:
            return abort(400)

        cat = models.Category.query.filter_by(id=id).first()

        if not cat:
            return abort(404)

        if cat not in g.user.categories:
            return abort(403)

        if not g.user.can_see(cat):
            return abort(403)

        cat.name = js['name']

        db.session.add(cat)
        db.session.commit()

        return jsonify(id=cat.id, name=cat.name)

    # Remove
    def delete(self, id):
        cat = models.Category.query.filter_by(id=id).first()

        if not cat:
            return abort(404)

        if cat not in g.user.categories:
            return abort(403)

        db.session.delete(cat)
        db.session.commit()

        return jsonify(id=cat.id)


class TagGroup(MethodView):

    # Retrieve
    # All if no ID; else one
    def get(self, cat_id, id):
        cat = models.Category.query.filter_by(id=cat_id).first()

        if not cat:
            return abort(404)

        if cat not in g.user.categories:
            return abort(403)

        if id is None:
            dic = {str(v.id): v.name for k, v in enumerate(models.TagGroup.query.filter_by(category=cat).all())}
            return jsonify(**dic)

        group = models.TagGroup.query.filter_by(category=cat, id=id).first()

        if not group:
            return abort(404)

        return jsonify(name=group.name)

    # Create
    def post(self, cat_id):
        js = request.get_json()

        if 'name' not in js:
            return abort(400)

        cat = models.Category.query.filter_by(id=cat_id).first()

        if not cat:
            return abort(400)

        if cat not in g.user.categories:
            return abort(403)

        group = models.TagGroup(js['name'], cat)

        db.session.add(group)
        db.session.commit()

        return jsonify(id=group.id, name=group.name)

    # Update
    def put(self, cat_id, id):
        js = request.get_json()

        if 'name' not in js:
            return abort(400)

        cat = models.Category.query.filter_by(id=cat_id).first()

        if not cat:
            return abort(404)

        if cat not in g.user.categories:
            return abort(403)

        grp = models.TagGroup.query.filter_by(category=cat, id=id).first()

        if not grp:
            return abort(400)

        grp.name = js['name']

        db.session.add(grp)
        db.session.commit()

        return jsonify(id=grp.id, name=grp.name)

    # Remove
    def delete(self, cat_id, id):
        cat = models.Category.query.filter_by(id=cat_id).first()

        if not cat:
            return abort(404)

        if cat not in g.user.categories:
            return abort(403)

        group = models.TagGroup.query.filter_by(category=cat, id=id).first()

        if not group:
            return abort(404)

        db.session.delete(group)
        db.session.commit()

        return jsonify(id=group.id)


class Tag(MethodView):

    # Retrieve
    # All if no ID; else one
    def get(self, cat_id, group_id, id):
        cat = models.Category.query.filter_by(id=cat_id).first()

        if not cat:
            return abort(404)

        if cat not in g.user.categories:
            return abort(403)

        group = models.TagGroup.query.filter_by(id=group_id, category=cat).first()

        if not group:
            return abort(404)

        if id is None:
            dic = {str(v.id): v.name for k, v in enumerate(models.Tag.query.filter_by(group=group).all())}
            return jsonify(**dic)

        tag = models.Tag.query.filter_by(category=cat, group=group, id=id).first()

        if not tag:
            return abort(404)

        return jsonify(name=tag.name)

    # Create
    def post(self, cat_id, group_id):
        js = request.get_json()

        if 'name' not in js:
            return abort(400)

        cat = models.Category.query.filter_by(id=cat_id).first()

        if not cat:
            return abort(400)

        if cat not in g.user.categories:
            return abort(403)

        grp = models.TagGroup.query.filter_by(category=cat, id=group_id).first()

        if not grp:
            return abort(400)

        tag = models.Tag(js['name'], grp)

        db.session.add(tag)
        db.session.commit()

        return jsonify(id=tag.id, name=tag.name)

    # Update
    def put(self, cat_id, group_id, id):
        js = request.get_json()

        if 'name' not in js:
            return abort(400)

        cat = models.Category.query.filter_by(id=cat_id).first()

        if not cat:
            return abort(400)

        if cat not in g.user.categories:
            return abort(403)

        grp = models.TagGroup.query.filter_by(category=cat, id=group_id).first()

        if not grp:
            return abort(400)

        tag = models.Tag.query.filter_by(group=grp, id=id).first()

        if not tag:
            return abort(404)

        tag.name = js['name']

        db.session.add(tag)
        db.session.commit()

        return jsonify(id=tag.id, name=tag.name)

    # Remove
    def delete(self, cat_id, group_id, id):
        cat = models.Category.query.filter_by(id=cat_id).first()

        if not cat:
            return abort(404)

        if cat not in g.user.categories:
            return abort(403)

        group = models.TagGroup.query.filter_by(category=cat, id=group_id).first()

        if not group:
            return abort(404)

        tag = models.Tag.query.filter_by(category=cat, group=group, id=id).first()

        if not tag:
            return abort(404)

        db.session.delete(tag)
        db.session.commit()

        return jsonify(id=tag.id)


class ImageSet(MethodView):

    # Retrieve
    # All if no ID; else one
    def get(self, cat_id, id):
        cat = models.Category.query.filter_by(id=cat_id).first()

        if not cat:
            return abort(404)

        if cat not in g.user.categories:
            return abort(403)

        if id is None:
            dic = {str(v.id): v for k, v in enumerate(models.ImageSet.query.filter_by(category=cat).all())}
            return jsonify(**dic)

        imgset = models.ImageSet.query.filter_by(category=cat, id=id).first()

        if not imgset:
            return abort(404)

        return jsonify(name=imgset.name)

    # Create
    def post(self, cat_id):
        pass

    # Update
    def put(self, cat_id, id):
        pass

    # Remove
    def delete(self, cat_id, id):
        cat = models.Category.query.filter_by(id=cat_id).first()

        if not cat:
            return abort(404)

        if cat not in g.user.categories:
            return abort(403)

        image_set = models.ImageSet.query.filter_by(category=cat, id=id).first()

        if not image_set:
            return abort(404)

        return jsonify(id=image_set.id)


class Image(MethodView):

    # Retrieve
    # All if no ID; else one
    def get(self, cat_id, id):
        cat = models.Category.query.filter_by(id=cat_id).first()

        if not cat:
            return abort(404)

        if cat not in g.user.categories:
            return abort(403)

        if id is None:
            dic = {str(v.id): hexlify(v.sha512).decode() for k, v in enumerate(models.Image.query.filter_by(category=cat).all())}
            return jsonify(**dic)

        img = models.Image.query.filter_by(category=cat, id=id).first()

        if not img:
            return abort(404)

        dic = {}
        if img.set:
            dic = {'set': img.set.id, 'rank': img.rank}

        return jsonify(sha512=hexlify(img.sha512).decode(),
                       category=img.category.id,
                       nsfw=img.nsfw.id,
                       type=img.type,
                       uploader=img.uploader.id,
                       added=img.added,
                       **dic)

    # Create
    def post(self, cat_id):
        cat = models.Category.retrieve(id=cat_id)

        if not cat:
            return abort(400)

        if cat not in g.user.categories:
            return abort(403)

        img_file = request.files['image']
        info = json.loads(request.files['json'].read().decode(encoding='UTF-8'))

        nsfw = info['nsfw']
        client_hsh = info['sha512']

        _set = info['set'] if 'set' in info else None
        rank = info['rank'] if 'set' in info else None

        hsh = sha512()
        hsh.update(img_file.read())
        img_file.seek(0)

        hsh_hex = hsh.hexdigest()

        if client_hsh != hsh_hex:
            return abort(400)

        digest = hsh.digest()

        img = models.Image.query.filter_by(sha512=digest).first()

        # Image already exists with that hash
        if img:
            return abort(500)

        tags = [models.Tag.retrieve(
                    name=tag['tag'],
                    group=models.TagGroup.retrieve(name=tag['group'], category=cat)) for tag in info['tags']]

        if any(x is None for x in tags):
            return abort(400)

        nsfw = models.NSFWStatus.retrieve(id=nsfw)

        if not nsfw:
            return abort(400)

        image_set = None
        if _set:
            image_set = models.ImageSet.retrieve(name=_set, category=cat)
            if not image_set or image_set.contains_element_of_rank(rank):
                return abort(400)

        img = models.Image(digest, info['type'], cat, nsfw, g.user, set=image_set, rank=rank)

        img.tags.extend(tags)

        db.session.add(img)
        db.session.commit()

        real_path = img.resolve_path(prefix=current_app.config['IMAGES_PREFIX'], create=True)

        img_file.save(real_path)

        return jsonify(id=img.id)

    # Update
    def put(self, cat_id, id):
        pass

    # Remove
    def delete(self, cat_id, id):
        cat = models.Category.query.filter_by(id=cat_id).first()

        if not cat:
            return abort(404)

        if cat not in g.user.categories:
            return abort(403)

        img = models.Image.query.filter_by(category=cat, id=id).first()

        if not img:
            return abort(404)

        img.delete_file(prefix=current_app.config['IMAGES_PREFIX'])
        db.session.delete(img)
        db.session.commit()

        return jsonify(img.id)


class ImageData(MethodView):

    def get(self, cat_id, id):
        cat = models.Category.query.filter_by(id=cat_id).first()

        if not cat:
            return abort(404)

        if cat not in g.user.categories:
            return abort(403)

        img = models.Image.query.filter_by(category=cat, id=id).first()

        if not img:
            return abort(404)

        if img.has_file(prefix=current_app.config['IMAGES_PREFIX']):
            return send_file(img.resolve_path(prefix=os.path.join(os.getcwd(), current_app.config['IMAGES_PREFIX'])))

        return abort(402)

    def put(self, cat_id, id):
        pass


class NSFWStatus(MethodView):

    # Retrieve
    def get(self):
        dic = {str(v.id): v.status for k, v in enumerate(models.NSFWStatus.query.all())}
        return jsonify(**dic)


def register_api(bp, view, endpoint, url):
    view_func = authed(view.as_view(endpoint))
    bp.add_url_rule(url, defaults={'id': None},
                    view_func=view_func, methods=['GET'])
    bp.add_url_rule(url, view_func=view_func, methods=['POST'])
    bp.add_url_rule('%s<int:id>' % (url,), view_func=view_func,
                    methods=['GET', 'PUT', 'DELETE'])


def get_blueprint():
    api = Blueprint('api', __name__)

    # hook up routing
    nsfw_view = NSFWStatus.as_view('nsfw_statuses')
    api.add_url_rule('/status/nsfw', view_func=nsfw_view)

    # May wish to add limit parameter to GET-all requests to paginate
    register_api(api, Category, 'category', '/categories/')
    register_api(api, TagGroup, 'tag_group', '/categories/<int:cat_id>/groups/')
    register_api(api, Tag, 'tag', '/categories/<int:cat_id>/groups/<int:group_id>/tags/')
    register_api(api, ImageSet, 'set', '/categories/<int:cat_id>/sets/')
    register_api(api, Image, 'image_meta', '/categories/<int:cat_id>/images/')

    image_data_view = ImageData.as_view('image_data')
    api.add_url_rule('/categories/<int:cat_id>/images/<int:id>/data/', view_func=authed(image_data_view))

    return api