from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
from flask.ext.httpauth import HTTPBasicAuth

db = SQLAlchemy()
login = LoginManager()
auth = HTTPBasicAuth()

import ryuuko.api as api
import ryuuko.html as html
import ryuuko.admin as admin


def init_app(pyfile=None):
    app = Flask(__name__)

    app.register_blueprint(api.get_blueprint(), url_prefix='/api')
    app.register_blueprint(html.get_blueprint())
    app.register_blueprint(admin.get_blueprint(), url_prefix='/admin')

    if pyfile:
        app.config.from_pyfile(pyfile, silent=True)

    db.init_app(app)
    login.init_app(app)

    return app