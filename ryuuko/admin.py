from flask import Blueprint
from flask.views import MethodView


class IndexView(MethodView):
    def get(self):
        pass


# TODO: Might want to rewrite this to better integrate with WTForms
class AddUserView(MethodView):
    def get(self):
        pass


class UserControlView(MethodView):
    def get(self):
        pass


def get_blueprint():
    admin = Blueprint('admin', __name__, template_folder='templates/admin')

    return admin