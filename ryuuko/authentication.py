from ryuuko import auth
import ryuuko.models as models
from flask import g, abort
from functools import wraps

@auth.verify_password
def verify_password(username, password):
    user = models.User.query.filter_by(name=username).first()

    if user:
        return user.verify(password)

    return False


def authed(f, perms=None):
    @wraps(f)
    def wrapper(*args, **kwargs):
        user = models.User.query.filter_by(name=auth.username()).first()

        # if not user.approved:
        #    return abort(403)

        if perms:
            rights = models.AccessRights.query.filter_by(rights=perms).first()

            if not rights:
                raise BaseException

            if user.rights.authority < rights.authority:
                return abort(403)


        g.user = user
        result = f(*args, **kwargs)
        user = getattr(g, 'user', None)

        if user:
            g.user = None

        return result

    return auth.login_required(wrapper)