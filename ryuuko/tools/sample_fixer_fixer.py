#/bin/env python

import hashlib
import os.path
import sys
import datetime
import re
import gelbooru
from colorama import init, Fore
import requests


def hashfile(afile, hasher, blocksize=65536):
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.hexdigest()


def main():
    init()
    matcher = re.compile('^(.*?)\.(.*?)$')
    not_found = []
    download_failed = []
    all_files = []

    for root, dirs, files in os.walk(sys.argv[1]):
        for f in files:
            full_path = os.path.join(root, f)
            ctime = datetime.datetime.fromtimestamp(os.path.getctime(full_path))
            if ctime > datetime.datetime(2015,2,1,0,0,0):
                hsh = hashfile(open(full_path, 'rb'), hashlib.md5())
                md5, ext = matcher.match(f).group(1,2)

                # not a real MD5; skip
                if len(md5) != 32:
                    continue

                if md5 != hsh:
                    print(Fore.CYAN + "[info]" + Fore.RESET + " {0} - {1}".format(md5, hsh))
                    all_files.append(full_path)

                    # Perform search
                    post = gelbooru.search(md5=md5)

                    # If no such MD5 is found
                    if len(post) < 1:
                        print(Fore.RED + "[err]" + Fore.RESET + " - {0}: Not found".format(md5))
                        not_found.append(full_path)
                        continue

                    # Get post data
                    img = post[0].image
                    print(Fore.GREEN + "[ok]" + Fore.RESET + " - {0}: {1}x{2} pixels; found".format(md5, img.width, img.height))

                    # Download image
                    img_data = requests.get(img.url, stream=True)
                    if img_data.status_code != 200:
                        download_failed.append(full_path)
                        print(Fore.RED + "[err]" + Fore.RESET + " - {0}: Download failed".format(md5))
                        continue

                    with open(os.path.join(root, "{0}.{1}".format(md5, ext)), 'wb') as f:
                        for chunk in img_data.iter_content(1024):
                            f.write(chunk)

                    print(Fore.GREEN + "[ok]" + Fore.RESET + " - {0}: Downloaded successfully".format(md5))

    if len(not_found) > 0:
        print(Fore.RED + "[err]" + Fore.RESET + " Some MD5s were not found")
        with open("not_found.log", 'w') as f:
            for md5 in not_found:
                print(md5, file=f)

    if len(download_failed) > 0:
        print(Fore.RED + "[err]" + Fore.RESET + " Some MD5s failed to download")
        with open("failed.log", 'w') as f:
            for md5 in download_failed:
                print(md5, file=f)

    with open("all.log", 'w') as f:
        for md5 in all_files:
            print(md5, file=f)

if __name__ == "__main__":
    main()