#/bin/env python

import os
import os.path
import sys
import fnmatch
import re
import gelbooru
from colorama import init, Fore
import requests


def main():
    init()
    matcher = re.compile('^sample_(.*?)\.(.*?)$')
    not_found = []
    download_failed = []

    print(Fore.CYAN + "[info]" + Fore.RESET + ' Searching in \'{0}\''.format(sys.argv[1]))
    for root, dirs, files in os.walk(sys.argv[1]):
        for filename in fnmatch.filter(files, 'sample_*.*'):
            md5, ext = matcher.match(filename).group(1,2)

            # not a real MD5; skip
            if len(md5) != 32:
                continue

            print(Fore.CYAN + "[info]" + Fore.RESET + " Searching for {0}...".format(md5))

            # Perform search
            post = gelbooru.search(md5=md5)

            # If no such MD5 is found
            if len(post) < 1:
                print(Fore.RED + "[err]" + Fore.RESET + " - {0}: Not found".format(md5))
                not_found.append(md5)
                continue

            # Get post data
            img = post[0].image
            print(Fore.GREEN + "[ok]" + Fore.RESET + " - {0}: {1}x{2} pixels; found".format(md5, img.width, img.height))

            # Download image
            img_data = requests.get(img.url, stream=True)
            if img_data.status_code != 200:
                download_failed.append(md5)
                print(Fore.RED + "[err]" + Fore.RESET + " - {0}: Download failed".format(md5))
                continue

            with open(os.path.join(root, "{0}.{1}".format(md5, ext)), 'wb') as f:
                for chunk in img_data.iter_content(1024):
                    f.write(chunk)

            os.unlink(os.path.join(root, filename))

            print(Fore.GREEN + "[ok]" + Fore.RESET + " - {0}: Downloaded successfully".format(md5))

    if len(not_found) > 0:
        print(Fore.RED + "[err]" + Fore.RESET + " Some MD5s were not found")
        with open("not_found.log", 'w') as f:
            for md5 in not_found:
                print(md5, file=f)

    if len(download_failed) > 0:
        print(Fore.RED + "[err]" + Fore.RESET + " Some MD5s failed to download")
        with open("failed.log", 'w') as f:
            for md5 in download_failed:
                print(md5, file=f)


if __name__ == "__main__":
    main()
