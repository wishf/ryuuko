import argparse
from ryuuko.client.instance import Ryuuko


def split_object(input, expected=2):
    parts = input.split(':')

    if len(parts) != expected:
        raise BaseException

    return parts


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('-c', '--categories',
                        action='store_true')
    parser.add_argument('-g', '--groups',
                        action='store_true')
    parser.add_argument('-s', '--sets',
                        action='store_true')
    parser.add_argument('-t', '--tags',
                        action='store_true')

    parser.add_argument('-C', '--add-category', default=None)
    parser.add_argument('-G', '--add-group', default=None)
    parser.add_argument('-S', '--add-sets', default=None)
    parser.add_argument('-T', '--add-tags', default=None)

    parser.add_argument('url')
    parser.add_argument('username')
    parser.add_argument('password')

    namespace = parser.parse_args()

    inst = Ryuuko(namespace.username, namespace.password, namespace.url)

    if namespace.add_category:
        inst.add_category(namespace.add_category)
        print("Added category")

    if namespace.add_group:
        namespace.add_group = split_object(namespace.add_group)

    if namespace.add_sets:
        namespace.add_sets = split_object(namespace.add_sets)

    if namespace.add_tags:
        namespace.add_tags = split_object(namespace.add_tags, expected=3)

    categories = inst.categories()

    for k, v in categories.items():
        if namespace.categories:
            print('Category \'{0}\': ID {1}'.format(v, k))

        category = inst.category(int(k))

        if namespace.add_group and v == namespace.add_group[0]:
            category.add_group(namespace.add_group[1])

        if namespace.add_sets and v == namespace.add_sets[0]:
            category.add_set(namespace.add_sets[1])

        add_tags = namespace.add_tags and v == namespace.add_tags[0]

        groups = category.groups()

        for k, v in groups.items():
            if namespace.groups:
                print('- Group \'{0}\': ID {1}'.format(v, k))

            group = category.group(int(k))

            if add_tags and v == namespace.add_tags[1]:
                group.add_tag(namespace.add_tags[2])

            tags = group.tags()

            for k, v in tags.items():
                if namespace.tags:
                    print(' -- Tag \'{0}\': ID {1}'.format(v, k))


        sets = category.sets()

        for k, v in sets.items():
            if namespace.sets:
                print(' - Set \'{0}\': ID {1}'.format(v, k))
