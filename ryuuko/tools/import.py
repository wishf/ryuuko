#/bin/env python

import os
import os.path
import re
import gelbooru
from colorama import init, Fore
import hashlib
from ryuuko.client.instance import Ryuuko
import argparse


fn_matcher = re.compile('(png|jpg|jpeg|gif|webm|bmp)$')


def import_all(base_dir, empty_tags, default_nsfw, category, include_dirs=True, import_gelbooru=True):
    # Set up tag groups as required
    groups = []

    if include_dirs:
        groups.append('dir')

    if import_gelbooru:
        groups.extend(['gel_artist', 'gel_copyright', 'gel_general', 'gel_character'])

    groups = retrieve_groups(groups, category.groups(), category)
    lookup = {k: {} for k, v in groups.items()}
    unprocessed_tags = {k: v.tags() for k, v in groups.items()}

    # Walk filesystem tree
    for current, dirs, files in os.walk(base_dir):
        if include_dirs:
            if current not in lookup['dir']:
                lookup['dir'][current] = retrieve_tag(current, unprocessed_tags['dir'], groups['dir'])

            dir_tag = lookup['dir'][current]

        for file in files:
            match = fn_matcher.search(file)

            if match:
                tags = []
                nsfw = default_nsfw

                if import_gelbooru:
                    with open(file, 'rb') as img:
                        md5 = hashfile(img, hashlib.md5())

                    post = gelbooru.search(md5=md5)

                    if len(post) != 1:
                        print(Fore.RED + "[err]" + Fore.RESET + " - {0}: Not found".format(md5))
                    else:
                        nsfw = post[0].rating
                        for tag in post[0].tags:
                            group = 'gel_{0}'.format(tag.type)
                            if tag.name not in lookup[group]:
                                lookup[group][tag.name] = retrieve_tag(tag.name, unprocessed_tags[group], groups[group])

                            tags.append(lookup[group][tag.name])

                if include_dirs:
                    tags.append(dir_tag)

                if not tags:
                    tags = empty_tags

                category.add_image(os.path.join(current, file), nsfw, tags)
                print(Fore.CYAN + "[info]" + Fore.RESET + " - Sent '{0}'".format(file))


def retrieve_groups(names, groups, category):
    if not names:
        return {}

    exists = [None for _ in names]

    for k, v in groups.items():
        for i, name in enumerate(names):
            if v == name:
                exists[i] = int(k)

    for i, name in enumerate(names):
        if not exists[i]:
            exists[i] = category.add_group(name)
        else:
            exists[i] = category.group(id=exists[i])

    return {k:v for k,v in zip(names, exists)}


def retrieve_tag(name, tags, group):
    exists = None

    for k, v in tags.items():
        if v == name:
            exists =int(k)

    if not exists:
        exists = group.add_tag(name)
        print(Fore.CYAN + "[info]" + Fore.RESET + " - Created tag '{0}:{1}'".format(group.name, name))
    else:
        exists = group.tag(id=exists)

    return exists


def hashfile(afile, hasher, blocksize=65536):
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.hexdigest()


def main():
    init()

    parser = argparse.ArgumentParser()

    parser.add_argument('address')
    parser.add_argument('name')
    parser.add_argument('password')

    parser.add_argument('category')

    parser.add_argument('root_dir')
    parser.add_argument('-D', '--tag-directories', action='store_true')
    parser.add_argument('-G', '--tag-gelbooru', action='store_true')

    namespace = parser.parse_args()

    instance = Ryuuko(namespace.name, namespace.password, namespace.address)

    nsfw = instance.nsfw_statuses()

    # Get NSFW id for 'unclassified'
    nsfw_id = 1
    for k, v in nsfw.items():
        if v == 'unclassified':
            nsfw_id = int(k)

    # Load category
    exists = None

    for k, v in instance.categories().items():
        if v == namespace.category:
            exists = int(k)

    if not exists:
        category = instance.add_category(namespace.category)
        print(Fore.CYAN + "[info]" + Fore.RESET + " - Created category '{0}'".format(namespace.category))
    else:
        category = instance.category(id=exists)

    default_group = retrieve_groups(['default'], category.groups(), category)['default']
    tagging_required_tag = retrieve_tag('tagging_required', default_group.tags(), default_group)

    import_all(namespace.root_dir,
               [tagging_required_tag],
               nsfw_id,
               category,
               include_dirs=namespace.tag_directories,
               import_gelbooru=namespace.tag_gelbooru)


if __name__ == "__main__":
    main()