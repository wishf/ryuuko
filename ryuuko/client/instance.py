import requests
from requests.auth import HTTPBasicAuth
from hashlib import sha512
import json
import os.path


class Ryuuko:
    def __init__(self, user, password, url):
        self.session = requests.Session()
        self.session.auth = HTTPBasicAuth(user, password)

        self.connection_url = url
        self._methods = {
            'GET': self.session.get,
            'PUT': self.session.put,
            'POST': self.session.post,
            'DELETE': self.session.delete
        }

    # TODO: Allow lookup by name
    def category(self, id=None):
        if not id:
            # TODO: Fully specify this exception
            raise BaseException

        # TODO: Use library to properly join URLs
        r = self.session.get(self.connection_url + '/categories/{0}'.format(id))

        if r.status_code != 200:
            # TODO: Fully specify this exception
            raise BaseException

        response = r.json()

        return Category(id, response['name'], self)

    def nsfw_statuses(self):
        # TODO: Use library to properly join URLs
        r = self.raw_relative_request('/status/nsfw')

        if r.status_code != 200:
            # TODO: Fully specify this exception
            raise BaseException

        return r.json()

    def categories(self):
        # TODO: Use library to properly join URLs
        r = self.raw_relative_request('/categories')

        if r.status_code != 200:
            # TODO: Fully specify this exception
            raise BaseException

        return r.json()

    def add_category(self, name):
        r = self.raw_relative_request('/categories/', json={'name': name}, mode='POST')

        if r.status_code != 200:
            # TODO: Fully specify the exception
            raise BaseException

        resp = r.json()

        return Category(resp['id'], resp['name'], self)

    def raw_relative_request(self, path, mode='GET', **kwargs):
        # TODO: Use library to properly join URLs
        return self._methods[mode](self.connection_url + path, **kwargs)


class Category:
    def __init__(self, id, name, parent):
        self.id = id
        self.name = name
        self.parent = parent

    def groups(self):
        r = self.raw_relative_request('/groups')

        if r.status_code != 200:
            # TODO: Fully specify this exception
            raise BaseException

        return r.json()

    # TODO: Allow lookup by name
    def group(self, id=None):
        if not id:
            # TODO: Fully specify this exception
            raise BaseException

        r = self.raw_relative_request('/groups/{0}'.format(id))

        if r.status_code != 200:
            # TODO: Fully specify this exception
            raise BaseException

        response = r.json()

        return TagGroup(id, response['name'], self)

    def add_group(self, name):
        r = self.raw_relative_request('/groups/', mode='POST', json={'name': name})

        if r.status_code != 200:
            # TODO: Fully specify this exception
            raise BaseException

        resp = r.json()

        return TagGroup(resp['id'], resp['name'], self)

    def sets(self):
        r = self.raw_relative_request('/sets')

        if r.status_code != 200:
            # TODO: Fully specify this exception
            raise BaseException

        return r.json()

    def set(self, id):
        if not id:
            # TODO: Fully specify this exception
            raise BaseException

        r = self.raw_relative_request('/sets/{0}'.format(id))

        if r.status_code != 200:
            # TODO: Fully specify this exception
            raise BaseException

        response = r.json()

        return ImageSet(id, response['name'], self)

    def add_set(self, name):
        r = self.raw_relative_request('/sets/', mode='POST', json={'name': name})

        if r.status_code != 200:
            # TODO: Fully specify this exception
            raise BaseException

        resp = r.json()

        return ImageSet(resp['id'], resp['name'], self)

    def add_image(self, image_file, nsfw, tags, _set=None, rank=None):
        hsh = sha512()

        with open(image_file, 'rb') as img:
            hsh.update(img.read())

        actual = hsh.hexdigest()

        _dict = {
            'nsfw': nsfw,
            'sha512': actual,
            'type': os.path.splitext(image_file)[1].strip('.'),
            'tags': [{'group': tag.parent.name, 'tag': tag.name} for tag in tags]
        }

        if _set:
            _dict['set'] = _set.name
            _dict['rank'] = rank

        jsonified = json.dumps(_dict)

        snd = {'image': open(image_file, 'rb'), 'json': jsonified.encode(encoding='UTF-8')}
        r = self.raw_relative_request('/images/', mode='POST', files=snd)


        if r.status_code != 200:
            # TODO: Fully specify this exception
            raise BaseException

        resp = r.json()

        return Image(resp['id'], self)

    def raw_relative_request(self, path, mode='GET', **kwargs):
        return self.parent.raw_relative_request('/categories/{0}{1}'.format(self.id, path), mode=mode, **kwargs)


class TagGroup:
    def __init__(self, id, name, parent):
        self.id = id
        self.name = name
        self.parent = parent

    def tags(self):
        # TODO: Use Library to properly join URLs
        r = self.raw_relative_request('/tags')

        if r.status_code != 200:
            # TODO: Fully specify this exception
            raise BaseException

        return r.json()

    def tag(self, id=None):
        if not id:
            raise BaseException

        r = self.raw_relative_request('/tags/{0}'.format(id))

        if r.status_code != 200:
            raise BaseException

        response = r.json()

        return Tag(id, response['name'], self)

    def add_tag(self, name):
        r = self.raw_relative_request('/tags/', mode='POST', json={'name': name})

        if r.status_code != 200:
            # TODO: Fully specify this exception
            raise BaseException

        response = r.json()

        return Tag(response['id'], response['name'], self)

    def raw_relative_request(self, path, mode='GET', **kwargs):
        return self.parent.raw_relative_request('/groups/{0}{1}'.format(self.id, path), mode=mode, **kwargs)


class Tag:
    def __init__(self, id, name, parent):
        self.id = id
        self.name = name
        self.parent = parent


class ImageSet:
    def __init__(self, id, name, parent):
        self.id = id
        self.name = name
        self.parent = parent


class Image:
    def __init__(self, id, parent):
        self.id = id
        self.parent = parent
