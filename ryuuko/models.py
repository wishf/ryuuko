from ryuuko import db
from binascii import hexlify
import os
import os.path
from datetime import datetime, timezone
from ryuuko.security import password_context


image_tags = db.Table('image_tags',
    db.Column('id', db.Integer, primary_key=True),
    db.Column('tag_id', db.Integer, db.ForeignKey('tags.id', ondelete='CASCADE')),
    db.Column('image_id', db.Integer, db.ForeignKey('images.id', ondelete='CASCADE'))
)

category_access = db.Table('category_access',
    db.Column('id', db.Integer),
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('category_id', db.Integer, db.ForeignKey('categories.id'))
)


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), nullable=False, unique=True, index=True)
    password = db.Column(db.String(130), nullable=False)

    rights_id = db.Column(db.Integer, db.ForeignKey('access_rights.id'), nullable=False)
    rights = db.relationship('AccessRights')

    categories = db.relationship('Category', secondary=category_access)

    approved = db.Column(db.Boolean, nullable=False, index=True)

    def __init__(self, name, password, rights):
        self.name = name
        self.password = password_context.encrypt(password)
        self.rights = rights
        self.approved = False

    def approve(self):
        self.approved = True

    def verify(self, password):
        return password_context.verify(password, self.password)


class AccessRights(db.Model):
    __tablename__ = 'access_rights'
    
    id = db.Column(db.Integer, primary_key=True)
    rights = db.Column(db.String(60), nullable=False)
    authority = db.Column(db.Integer, nullable=False)

    def __init__(self, rights, authority):
        self.rights = rights
        self.authority = authority


class Image(db.Model):
    __tablename__ = 'images'

    id = db.Column(db.Integer, primary_key=True)
    sha512 = db.Column(db.LargeBinary, nullable=False, unique=True)
    type = db.Column(db.String(10), nullable=False)

    category_id = db.Column(db.Integer, db.ForeignKey('categories.id'),
        nullable=False)
    category = db.relationship('Category',
        backref=db.backref('images', lazy='dynamic'))

    added_posix = db.Column(db.Integer, nullable=False)

    set_id = db.Column(db.Integer, db.ForeignKey('sets.id'))
    set = db.relationship('ImageSet',
        backref=db.backref('images', lazy='dynamic'))
    rank = db.Column(db.Integer)

    nsfw_id = db.Column(db.Integer, db.ForeignKey('nsfw_statuses.id'),
        nullable=False)
    nsfw = db.relationship('NSFWStatus')

    uploader_id = db.Column(db.Integer, db.ForeignKey('users.id'),
        nullable=False)
    uploader = db.relationship('User',
        backref=db.backref('uploads', lazy='dynamic'))

    tags = db.relationship('Tag', secondary=image_tags,
        backref=db.backref('images', lazy='dynamic'))

    def __init__(self,
                 sha512,
                 type,
                 category,
                 nsfw,
                 uploader,
                 set=None,
                 rank=None):
        self.sha512 = sha512
        self.category = category
        self.type = type
        self.nsfw = nsfw
        self.uploader = uploader

        self.added_posix = datetime.now(timezone.utc).timestamp()

        if set:
            self.set = set
            self.rank = rank

    def resolve_path(self, prefix='', create=False):
        hsh_hex = hexlify(self.sha512).decode(encoding='UTF-8')

        dest_name ='%s.%s' % (hsh_hex[2:], self.type)

        first = os.path.join(prefix, '{0}/'.format(hsh_hex[0]))
        if create and not os.path.exists(first):
            os.mkdir(first)

        second = os.path.join(first, '{0}/'.format(hsh_hex[1]))
        if create and not os.path.exists(second):
            os.mkdir(second)

        print(os.path.join(second, dest_name))
        return os.path.join(second, dest_name)

    def open(self, mode, prefix='', create=False):
        return open(self.resolve_path(self, prefix=prefix, create=create), mode)

    def delete_file(self, prefix=''):
        path = self.resolve_path(prefix=prefix)

        if os.path.exists(path):
            os.remove(path)

    def has_file(self, prefix=''):
        return os.path.exists(self.resolve_path(prefix=prefix))

    @property
    def added(self):
        return datetime.utcfromtimestamp(self.added_posix)


class NSFWStatus(db.Model):
    __tablename__ = 'nsfw_statuses'

    id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.String(60), nullable=False)

    def __init__(self, status):
        self.status = status

    @staticmethod
    def retrieve(id=None, name=None):
        if name:
            return NSFWStatus.query.filter_by(name=name).first()
        elif id:
            return NSFWStatus.query.filter_by(id=id).first()
        else:
            raise BaseException


class Tag(db.Model):
    __tablename__ = 'tags'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))

    group_id = db.Column(db.Integer, db.ForeignKey('tag_groups.id'),
        nullable=False)
    group = db.relationship('TagGroup',
        backref=db.backref('tags', lazy='dynamic'))

    __table_args__ = (db.Index('tag_name_group_id', 'name', 'group_id', unique=True), )

    def __init__(self, name, group):
        self.name = name
        self.group = group

    @staticmethod
    def retrieve(id=None, name=None, group=None, create=False):
        if name and group:
            tag = Tag.query.filter_by(name=name, group=group).first()

            if not tag and create:
                tag = Tag(name, group)

                db.session.add(tag)
                db.session.commit()

            return tag

        elif id:
            return Tag.query.filter_by(id=id).first()
        else:
            raise BaseException


class Category(db.Model):
    __tablename__ = 'categories'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True, index=True)

    def __init__(self, name):
        self.name = name

    @staticmethod
    def retrieve(id=None, name=None, create=False):
        if name:
            cat = Category.query.filter_by(name=name).first()

            if not cat and create:
                cat = Category(name)

                db.session.add(cat)
                db.session.commit()

            return cat

        elif id:
            return Category.query.filter_by(id=id).first()
        else:
            raise BaseException


class ImageSet(db.Model):
    __tablename__ = 'sets'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))

    category_id = db.Column(db.Integer, db.ForeignKey('categories.id'),
        nullable=False)
    category = db.relationship('Category',
        backref=db.backref('sets', lazy='dynamic'))

    __table_args__ = (db.Index('set_name_cat_id', 'name', 'category_id', unique=True), )

    def __init__(self, name, category):
        self.name = name
        self.category = category

    @staticmethod
    def retrieve(id=None, name=None, category=None, create=False):
        if name and category:
            iset = ImageSet.query.filter_by(name=name, category=category).first()

            if not iset and create:
                iset = ImageSet(name, category)

                db.session.add(iset)
                db.session.commit()

            return iset

        elif id:
            return ImageSet.query.filter_by(id=id).first()
        else:
            raise BaseException


class TagGroup(db.Model):
    __tablename__ = 'tag_groups'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))

    category_id = db.Column(db.Integer, db.ForeignKey('categories.id'),
        nullable=False)
    category = db.relationship('Category',
        backref=db.backref('groups', lazy='dynamic'))

    def __init__(self, name, category):
        self.name = name
        self.category = category

    @staticmethod
    def retrieve(id=None, name=None, category=None, create=False):
        if name and category:
            tg = TagGroup.query.filter_by(name=name, category=category).first()

            if not tg and create:
                tg = TagGroup(name, category)

                db.session.add(tg)
                db.session.commit()

            return tg

        elif id:
            return TagGroup.query.filter_by(id=id).first()
        else:
            raise BaseException