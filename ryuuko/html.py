from flask import Blueprint, render_template
from flask.views import MethodView
from ryuuko import auth


class IndexView(MethodView):
    def get(self):
        return render_template('index.html')


class CategoriesView(MethodView):
    def get(self):
        return render_template('categories.html')


class TagsView(MethodView):
    def get(self, group_id, category_id):
        return render_template('tags.html')


class GroupsView(MethodView):
    def get(self, category_id):
        return render_template('groups.html')


class SearchView(MethodView):
    def get(self, category_id):
        pass

    def post(self, category_id):
        pass


class UserView(MethodView):
    def get(self, user_id):
        return render_template('user.html')


class ImageView(MethodView):
    def get(self, image_id):
        return render_template('image.html')


def get_blueprint():
    html = Blueprint('html', __name__, template_folder='templates/html')

    # hook up routing
    index = IndexView.as_view('index')
    html.add_url_rule('/', view_func=index)

    cats = CategoriesView.as_view('categories')
    html.add_url_rule('/categories', view_func=cats)

    tags = TagsView.as_view('tags')
    html.add_url_rule('/category/<int:category_id>/group/<int:group_id>/tags', view_func=tags)
    html.add_url_rule('/category/<int:category_id>/tags', defaults={'group_id': None}, view_func=tags)

    groups = GroupsView.as_view('groups')
    html.add_url_rule('/category/<int:category_id>/groups', view_func=groups)

    user = UserView.as_view('user')
    html.add_url_rule('/user/<int:user_id>', view_func=user)

    image = ImageView.as_view('image')
    html.add_url_rule('/image/<int:image_id>', view_func=image)

    search = SearchView.as_view('search')
    html.add_url_rule('/category/<int:category_id>/images', view_func=search)

    return html